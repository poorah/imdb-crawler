#!/usr/bin/env python3
"""! @brief IMDB Crawler Project"""

##
# @mainpage IMDB Crawler Project Document
#
# @section description_main Description
# IMDB Crawler project for learning python/django Project
#
# @section notes_main Notes
# - Developed by Pooya Rahmanian in July 2022 supervised by Mahmoud Kiani
#
# Copyright (c) 2022 HadidTech.  All rights reserved.
##

##
# @file init.py
from .utils.spectacular_extentions import MyAuthenticationScheme
