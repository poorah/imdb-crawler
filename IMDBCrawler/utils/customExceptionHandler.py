from rest_framework.views import exception_handler
from django.http import Http404
from rest_framework.exceptions import ValidationError, PermissionDenied, NotAuthenticated
from django.utils.translation import gettext as _


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if isinstance(exc, Http404):
        custom_response_data = {
            _('error'): '{id} not found'.format(id=context['kwargs']['pk'])
        }
        # print(f'exc: {type(exc)}')
        # print(f'context:{context}')
        # print(f'Http404:{Http404}')
        response.data = custom_response_data

    if isinstance(exc, ValidationError):
        custom_response_data = {
            _('error'): _('fields are not valid')
        }
        response.data = custom_response_data

    if isinstance(exc, PermissionDenied):
        custom_response_data = {
            _('error'): _("you don't have permission to do this")
        }
        response.data = custom_response_data

    if isinstance(exc, NotAuthenticated):
        custom_response_data = {
            _('error'): _("you are not authenticated")
        }
        response.data = custom_response_data

    return response
