# IMDB crawler  

## Initializing the project  
1. you must have installed ```virtualenv ```on your system.if it's not installed run these commands:  
	- ```pip install virtualenv```  
	- ```virtualenv venv```  
	- ```source venv/bin/activate```  
	- ```pip install -r requirements.txt```  
  
2. choose your preferred settings (*development_settings is recommended*)  
   - In linux you can set that running this command:  
      ``export DJANGO_SETTINGS_MODULE=IMDBCrawler.development_settings``  
3. Set your postgres config secrets in .env file ( *you must have postgres on your system*)  
4. Set your email Host provider configs  
5. run migrations:  
   - ``python3 manage.py makemigrations``  
   - ``python3 manage.py migrate``  
6. create a superuser for accessing the admin panel:  
   - ``python3 manage.py createsuperuser``  
   - ``enter your email, your phone number and your password``  
7. now you can run the application:  
   - ``python3 manage.py runserver``  
    
## challenges  
  
- swagger:  
  
   If you are using serializer method field you will face some issues:  
  
   To fix it you, you have to use `@extend_schema_field(OpenApiTypes.methodfieldReturnType)`  
  
	 ```python  
	 from drf_spectacular.views import OpenApiTypes  
	 from drf_spectacular.utils import extend_schema_field  
	    
	 downloadlink_with_methodfield = serializers.SerializerMethodField() 

	 @extend_schema_field(OpenApiTypes.ANY)  
	 def get_downloadlink_with_methodfield(self, obj):  
	 	return 'http://127.0.0.1:8000/' + obj.file  
	 ```

- HTTP Request Methods:
	all name's letter must be in CAPITAL!! 
	for example ``request.method == 'put'`` is wrong.
	correct form is ``request.method == 'PUT'``

- Including all fields in serializer:
	you can do this: ``fields = '__all__'``
		
- APScheduler tutorial:
	https://dev.to/brightside/scheduling-tasks-using-apscheduler-in-django-2dbl
	
- Custom user model:
	don't forget to set ``AUTH_USER_MODEL = 'appName.CustomModel'`` in ``settings.py``

- Crawler twice downloading issue:  
   this is APScheduler bug. In order to fix it you have to runserver using this command:  
   ``python3 manage.py runserver --noreload``  
	``--noreload`` will prevent server to restart automatically

- you can learn more about project challenges in :
	https://1drv.ms/u/s!AniZhsGTO2IJgQZYHJ8g6Viu1oa-?e=Pbpc7t

## Bugs

- django jalali:
	- there was a bug in django jalali package. sometimes it returned a naive datetime format.
	  the problem was in django jalali/db/models.py in date_parse function
