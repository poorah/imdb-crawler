##
# @file cast/castdetailserializer.py
#
# @brief showing an actor/actress information.
#
# @section description_sensors Description
# a serializer for retrieving a single cast information
#
# @section libraries_cast Libraries/Modules
#  - Django restframework.
#


from rest_framework import serializers
from .models import Cast
from movie.models import Movie
from movie.serializer import MovieSerializer


class CastDetailSerializer(serializers.ModelSerializer):
    """a class for serializing a cast information"""

    ##movies that the cast has played in
    movies = MovieSerializer(many=True)

    class Meta:
        """class Meta"""

        ##model that we are using to retrieve data
        model = Cast
        ##fields that we want to retrieve from database
        fields = ['id', 'cast_name', 'movies']
