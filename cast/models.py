##
# @file cast/models.py
#
# @brief Cast app models.
#
# @section description_sensors Description
# Defines the cast models.
#
# @section libraries_cast Libraries/Modules
#   - models in django.db
#
# @section author_cast Author(s)
# - Created by Pooya Rahmanian on July 2022.
#
# Copyright (c) 2022 HadidTech.  All rights reserved.
from django.db import models


class Cast(models.Model):
    """Cast table schema"""

    ##name of the actor
    cast_name = models.CharField(max_length=256)

    def __str__(self):
        """function that just tells Django what to print when it needs to print out an instance of the any model.
           @return cast name(string)"""
        return self.cast_name
