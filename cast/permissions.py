##
# @file cast/permissions.py
#
# @brief custom permissions.
#
# @section description_cast Description
# Define a custom permission class
#
# @section libraries_custom Libraries/Modules
# - django rest framework
#
#
#

from rest_framework import permissions


class IsAdminOrReadonly(permissions.BasePermission):
    """A custom permission class that first checks whether the user is admin or not
     if he/she is admin, can do writing operations like creating new cast, updating an existing cast
     if not , he/she can do just reading operations like GET,HEAD,..."""

    def has_permission(self, request, view):
        """check if object has permission
        @param self referencing the current objects methods and properties
        @param request incoming request
        @param view view :)

        @return a boolean datatype that declare whether it has user permission or not
        """

        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.is_superuser
