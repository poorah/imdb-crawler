##
# @file cast/serializer.py
#
# @brief some serializers for cast app.
#
# @section description_cast Description
# Defines some serializers for showing all casts, creating a new cast and updating an existing cast
# - CastSerializer : for showing all casts
# - CreateCastSerializer : creating a new cast
# - UpdateCastSerializer : update an existing cast
#
# @section libraries_cast Libraries/Modules
# - django restframework
#
#
# @section todo_cast
# - change the PrimaryKeyRelatedField to MovieSerializer
#
# @section author_cast Author(s)
# - Created by Pooya Rahmanian
#
# Copyright (c) 2022 HadidTech.  All rights reserved.

from rest_framework import serializers
from .models import Cast
from movie.models import Movie


class CastSerializer(serializers.ModelSerializer):
    """a serializer class for retrieving all casts data and show them"""

    ##movies that casts have played in
    movies = serializers.PrimaryKeyRelatedField(many=True, queryset=Movie.objects.all())

    class Meta:
        """class Meta"""

        ##model that we are retrieving data from it
        model = Cast
        ##fields that we are going to send to client side
        fields = ['id', 'cast_name', 'movies']


class CreateCastSerializer(serializers.ModelSerializer):
    """a serializer class for creating a new cast """

    class Meta:
        """class Meta"""

        ##model that we are retrieving data from it
        model = Cast
        ##fields that we are going to send to client side
        fields = ['cast_name']


class UpdateCastSerializer(serializers.ModelSerializer):
    """a serializer class for updating an existing cast """

    class Meta:
        """class Meta"""

        ##model that we are retrieving data from it
        model = Cast
        ##fields that we are going to send to client side
        fields = ['cast_name']
