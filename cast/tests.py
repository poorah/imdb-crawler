from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from .models import Cast
from django.contrib.auth.models import User


class CastTest(APITestCase):

    def setUp(self):
        self.cast = Cast(
            cast_name='test'
        )
        self.cast.save()

        self.user = User.objects.create_user(username='pooya',
                                             password='1234',
                                             is_superuser=True)

    def test_show_all(self):
        url = reverse('cast_view')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIs(Cast.objects.count() >= 0, True)

    def test_show_a_cast_details(self):
        url = reverse('CastDetailView', args=(self.cast.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_new_cast(self):
        url = reverse('cast_view')
        data = {
            "cast_name": "test2"
        }
        response = self.client.post(url, data, format('json'))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_a_cast(self):
        url = reverse('CastDetailView', args=(self.cast.id,))
        data = {
            'cast_name': 'updated'
        }
        response = self.client.put(url, data, format('json'))
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

    def test_delete_a_cast_with_auth(self):
        url = reverse('CastDetailView', args=(self.cast.id,))
        self.client.login(username='pooya', password='1234')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.client.logout()

    def test_delete_a_cast_without_auth(self):
        url = reverse('CastDetailView', args=(self.cast.id,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
