from django.urls import path
from .views import CastView, CastDetailView
urlpatterns = [
    path('', CastView.as_view(), name='cast_view'),
    path('detail/<int:pk>', CastDetailView.as_view(), name='CastDetailView')
]