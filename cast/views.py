##
# @file cast/views.py
#
# @brief cast app view classes.
#
# @section description_cast Description
# view classes for cast app.
# - CastView : a class for showing all casts and create a new one
# - CastDetailView : a class for showing a specific cast, update it and delete it
#
# @section libraries_cast Libraries/Modules
# - django restframework
#   - generics
#   - Response
#   - status
#
#

from rest_framework import generics
from .serializer import CastSerializer, CreateCastSerializer, UpdateCastSerializer
from .models import Cast
from .castdetailserializer import CastDetailSerializer
from .permissions import IsAdminOrReadonly
from rest_framework.response import Response
from rest_framework import status
from rest_framework.throttling import UserRateThrottle


class CastView(generics.ListCreateAPIView):
    """a view class for showing all casts and creating a new one"""

    ##queryset :)
    queryset = Cast.objects.all()
    ##serializer class :)
    serializer_class = CastSerializer
    ##throttle class :)
    throttle_classes = [UserRateThrottle]
    ##permission class
    permission_classes = [IsAdminOrReadonly]

    def create(self, request, *args, **kwargs):
        """overriding create method

        @param self :)
        @param request
        @param *args
        @param **kwargs

        @return new instance's datas if data's where valid, if not it will
        return the errors
        """
        #todo: add admin permission to create method
        serializer = CreateCastSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CastDetailView(generics.RetrieveUpdateDestroyAPIView):
    """a view class for showing a specific cast, update and delete an existing one"""

    ##queryset :)
    queryset = Cast.objects.all()
    ##serializer_class :)
    serializer_class = CastDetailSerializer
    ##our custom permission class that we have wrote
    permission_classes = [IsAdminOrReadonly]

    def update(self, request, pk, *args, **kwargs):
        ##todo: add permission
        """overriding update method
            @param self :)
            @param request incoming request
            @param pk the id of the cast

            @return updated instance's datas if data's where valid, if not it will
            return the errors
            """

        cast = Cast.objects.get(pk=pk)
        serializer = UpdateCastSerializer(cast, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
