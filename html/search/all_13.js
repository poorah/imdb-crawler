var searchData=
[
  ['update_121',['update',['../classcast_1_1views_1_1CastDetailView.html#acc5deb0c5c8058595d912dd28b7344b5',1,'cast.views.CastDetailView.update()'],['../classmovie_1_1views_1_1MovieDetail.html#acc5deb0c5c8058595d912dd28b7344b5',1,'movie.views.MovieDetail.update()'],['../classpurchase_1_1views_1_1PurchaseDetailView.html#acc5deb0c5c8058595d912dd28b7344b5',1,'purchase.views.PurchaseDetailView.update()']]],
  ['updatecastserializer_122',['UpdateCastSerializer',['../classcast_1_1serializer_1_1UpdateCastSerializer.html',1,'cast::serializer']]],
  ['urls_2epy_123',['urls.py',['../movie_2urls_8py.html',1,'']]],
  ['user_124',['user',['../classpurchase_1_1serializer_1_1PurchaseDetailView.html#a5cc32e366c87c4cb49e4309b75f57d64',1,'purchase::serializer::PurchaseDetailView']]],
  ['useradminpermission_125',['UserAdminPermission',['../classpurchase_1_1admin_1_1UserAdminPermission.html',1,'purchase::admin']]],
  ['userdetailserializer_126',['UserDetailSerializer',['../classuser_1_1serializer_1_1UserDetailSerializer.html',1,'user::serializer']]],
  ['userdetailview_127',['UserDetailView',['../classuser_1_1views_1_1UserDetailView.html',1,'user::views']]],
  ['userlistcreate_128',['UserListCreate',['../classuser_1_1views_1_1UserListCreate.html',1,'user::views']]],
  ['userserializer_129',['UserSerializer',['../classuser_1_1serializer_1_1UserSerializer.html',1,'user::serializer']]]
];
