var searchData=
[
  ['permission_5fclasses_87',['permission_classes',['../classpurchase_1_1views_1_1PurchaseDetailView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'purchase.views.PurchaseDetailView.permission_classes()'],['../classpurchase_1_1views_1_1PurchaseView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'purchase.views.PurchaseView.permission_classes()'],['../classcast_1_1views_1_1CastView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'cast.views.CastView.permission_classes()'],['../classcast_1_1views_1_1CastDetailView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'cast.views.CastDetailView.permission_classes()'],['../classmovie_1_1views_1_1MovieDownloadLink.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'movie.views.MovieDownloadLink.permission_classes()']]],
  ['permissions_88',['permissions',['../classpurchase_1_1models_1_1Purchase_1_1Meta.html#aabddf8b80056231960da6ecded6bd07f',1,'purchase.models.Purchase.Meta.permissions()'],['../classmovie_1_1models_1_1Movie_1_1Meta.html#aabddf8b80056231960da6ecded6bd07f',1,'movie.models.Movie.Meta.permissions()']]],
  ['permissions_2epy_89',['permissions.py',['../cast_2permissions_8py.html',1,'(Global Namespace)'],['../purchase_2permissions_8py.html',1,'(Global Namespace)'],['../user_2permissions_8py.html',1,'(Global Namespace)']]],
  ['pernissions_2epy_90',['pernissions.py',['../pernissions_8py.html',1,'']]],
  ['post_91',['post',['../classuser_1_1views_1_1Login.html#a3058b14c69af78f91c3ed15ffaf7ff3f',1,'user::views::Login']]],
  ['price_92',['price',['../classmovie_1_1models_1_1Movie.html#a4dd5ee47dc4a408b73089ec6f8160083',1,'movie::models::Movie']]],
  ['purchase_93',['purchase',['../classuser_1_1serializer_1_1UserDetailSerializer.html#a04fa65d8705cc419d723e0576179bda6',1,'user::serializer::UserDetailSerializer']]],
  ['purchase_94',['Purchase',['../classpurchase_1_1models_1_1Purchase.html',1,'purchase::models']]],
  ['purchasecreateserializer_95',['PurchaseCreateSerializer',['../classpurchase_1_1serializer_1_1PurchaseCreateSerializer.html',1,'purchase::serializer']]],
  ['purchasedetailview_96',['PurchaseDetailView',['../classpurchase_1_1serializer_1_1PurchaseDetailView.html',1,'PurchaseDetailView'],['../classpurchase_1_1views_1_1PurchaseDetailView.html',1,'PurchaseDetailView']]],
  ['purchaseserializer_97',['PurchaseSerializer',['../classpurchase_1_1serializer_1_1PurchaseSerializer.html',1,'purchase::serializer']]],
  ['purchaseupdateserializer_98',['PurchaseUpdateSerializer',['../classpurchase_1_1serializer_1_1PurchaseUpdateSerializer.html',1,'purchase::serializer']]],
  ['purchaseview_99',['PurchaseView',['../classpurchase_1_1views_1_1PurchaseView.html',1,'purchase::views']]]
];
