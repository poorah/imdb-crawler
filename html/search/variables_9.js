var searchData=
[
  ['permission_5fclasses_253',['permission_classes',['../classcast_1_1views_1_1CastView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'cast.views.CastView.permission_classes()'],['../classcast_1_1views_1_1CastDetailView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'cast.views.CastDetailView.permission_classes()'],['../classmovie_1_1views_1_1MovieDownloadLink.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'movie.views.MovieDownloadLink.permission_classes()'],['../classpurchase_1_1views_1_1PurchaseView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'purchase.views.PurchaseView.permission_classes()'],['../classpurchase_1_1views_1_1PurchaseDetailView.html#ad1a31bf4d10343f31c581f635cef1b2e',1,'purchase.views.PurchaseDetailView.permission_classes()']]],
  ['permissions_254',['permissions',['../classmovie_1_1models_1_1Movie_1_1Meta.html#aabddf8b80056231960da6ecded6bd07f',1,'movie.models.Movie.Meta.permissions()'],['../classpurchase_1_1models_1_1Purchase_1_1Meta.html#aabddf8b80056231960da6ecded6bd07f',1,'purchase.models.Purchase.Meta.permissions()']]],
  ['price_255',['price',['../classmovie_1_1models_1_1Movie.html#a4dd5ee47dc4a408b73089ec6f8160083',1,'movie::models::Movie']]],
  ['purchase_256',['purchase',['../classuser_1_1serializer_1_1UserDetailSerializer.html#a04fa65d8705cc419d723e0576179bda6',1,'user::serializer::UserDetailSerializer']]]
];
