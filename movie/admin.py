##
# @file movie/admin.py
#
# @brief movie admin configs
#
# @section description_movie Description
# movie model admin panel configs
#
# @section libraries_movie Libraries/Modules
#  - Django.contrib.admin
#  - jalali_date.admin.ModelAdminJalaliMixin : persian calendar widget
##

from django.contrib import admin
from .models import Movie
from jalali_date.admin import ModelAdminJalaliMixin


class CastInline(admin.StackedInline):
    """a class for showing the movie's casts in admin panel"""

    ##casts in movie model
    model = Movie.casts.through


class MovieAdmin(ModelAdminJalaliMixin, admin.ModelAdmin):
    """a class for movie admin panel configs"""

    ##inline fields
    inlines = [
        CastInline,
    ]

    ##movie model
    movie = Movie()

    ##lists
    list_display = ('movie_name', 'get_download_link',)


admin.site.register(Movie, MovieAdmin)
