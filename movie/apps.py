##
# @file movie/apps.py
#
# @brief movie apps configs
#
# @section description_movie Description
# movie app configurations
#

from django.apps import AppConfig


class MovieConfig(AppConfig):
    """Movie app configs class"""

    ##default database primary key
    default_auto_field = 'django.db.models.BigAutoField'

    ##app name
    name = 'movie'

    def ready(self):
        """
        ready function to run signals when occurred

        @param self :)

        @return void
        """
        import movie.signals

