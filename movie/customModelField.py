from django.db import models
from django.core.exceptions import ValidationError


class ToomanField(models.IntegerField):
    description = "A field to save Tooman as dollar (int) in db"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_db_prep_value(self, value, *args, **kwargs):
        if value is None:
            return None
        return float(value / 30000)

    def to_python(self, value):
        if value is None:
            return value
        try:
            return value * 30000
        except (TypeError, ValueError):
            raise ValidationError("This value must be an integer")

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        return name, path, args, kwargs
