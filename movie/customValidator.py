##
# @file movie/customValidator.py
#
# @brief movie custom validator
#
# @section description_movie Description
# a custom price value validator that raise error if price is less than 0!
#
# @section libraries_movie Libraries/Modules
#  - Django.core.exceptions.ValidationError
##

from django.core.exceptions import ValidationError


def validate_price(value):
    """
    a function that checks if the price value is not negative

    @param[in,out] value movie price (int)

    @return void if value is less than 0 it will raise a ValidationError
    """
    if value < 0:
        raise ValidationError(f'{value} is less than 0')
