##
# @file movie/models.py
#
# @brief movie models
#
# @section description_movie Description
# movie model definition
#
# @section libraries_movie Libraries/Modules
#  - django.core.validators
#  - jalali_date
#  - django_jalali
#  - drf_spectacular
#  - drf_spectacular
##

from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from cast.models import Cast
from django.utils import timezone
import os
from django.conf import settings
from .customValidator import validate_price
from jalali_date import datetime2jalali, date2jalali
from django_jalali.db import models as jmodels
from drf_spectacular.utils import extend_schema_field
from drf_spectacular.views import OpenApiTypes
from .customModelField import ToomanField

# Create your models here.
class Movie(models.Model):
    """Movie model definitions"""

    ##name
    movie_name = models.CharField(max_length=256)

    ##ranking
    ranking = models.IntegerField(validators=[MinValueValidator(1),
                                              MaxValueValidator(250)])
    ##rating
    rating = models.FloatField(validators=[MinValueValidator(0.00),
                                           MaxValueValidator(10.00)])
    ##casts that have played in movie
    casts = models.ManyToManyField(Cast, related_name='movies')

    ##movie price
    price = models.IntegerField(validators=[validate_price])

    ##last update date and time in UTC format
    last_modification = models.DateTimeField()

    ##last update date and time in persian jalali format
    jdatetime = jmodels.jDateTimeField(default=timezone.now())

    ##movie download link
    download_link = models.URLField(null=True)

    ##movie's downloaded file resolution
    resouloution = models.CharField(max_length=10, default="720p")

    ##movie's file path
    file = models.FilePathField(path=os.path.join(settings.MOVIE_PATH))

    tooman = ToomanField()

    class Meta:
        """class meta"""

        ##ordering by field
        ordering = ['ranking']

        ##custom permissions
        permissions = [
            ('change_movie_name', 'Can change movie title')
        ]

    def __str__(self):
        """__str__ function :)
        @param self :)
        @return movie name string"""
        return self.movie_name

    def get_download_link(self):
        """create download link dynamically function
        @param self :)
        @return download link string"""
        return 'http://127.0.0.1:8000/' + self.file

    @extend_schema_field(OpenApiTypes.ANY)
    def to_jalali(self):
        """
        change last_modification field date format from gregorian to jalali
        @param self :)
        @return last modification date in jalali format"""

        return date2jalali(self.last_modification).strftime('%Y-%M-%d _ %H:%M:%S')
