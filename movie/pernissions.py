##
# @file movie/pernissions.py
#
# @brief movie custom permissions
#
# @section description_movie Description
# movie app custom permission classes
#
# @section libraries_movie Libraries/Modules
#  - rest_framework
##
from rest_framework import permissions
from purchase.models import Purchase


class HasUserBoughtVideo(permissions.BasePermission):
    """custom permission class that checks if the user has bought the video or not in order to
    show him the download link"""
    def has_object_permission(self, request, view, obj):
        """
        function that checks if user has permission or not

        @param request incoming request
        @param view :)
        @param obj current object

        @return True/False Boolean
        """
        user = request.user
        print(user)
        if Purchase.objects.filter(user=user.id, movie=obj).exists() or user.is_superuser == True:
            return True
        return False


class JustDestroyPermission(permissions.BasePermission):
    """a custom permission class that checks if user has permission to delete a movie or not"""
    def has_object_permission(self, request, view, obj):
        """
        a function that checks if user is superuser or not. if it was superuser it will grant permission

        @param self :)
        @param request incoming request
        @param view
        @param obj current object

        @return True/False
        """
        if request.method == 'DELETE':
            return request.user.is_superuser
        return True

# class CanChangeMovieName(permissions.BasePermission):
#
#     def has_object_permission(self, request, view, obj):
#         if request.method == 'PUT'
