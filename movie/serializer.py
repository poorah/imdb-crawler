##
# @file movie/serializer.py
#
# @brief movie serializers
#
# @section description_movie Description
# serializers for working with movie model's
#
# @section libraries_movie Libraries/Modules
#  - Django restframework.
#  - drf_spectacular: for swagger documentation
#  - django_jalali: persian calendar

from rest_framework import serializers
from .models import Movie
from drf_spectacular.views import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from django_jalali.serializers.serializerfield import JDateTimeField


class MovieSerializer(serializers.ModelSerializer):
    """a class for serializing movies brief information in order to show them as a list"""

    class Meta:
        """class Meta"""

        ##model
        model = Movie

        ##fields
        fields = ['id', 'movie_name', 'ranking', 'rating', 'price',
                  'last_modification', 'jdatetime', 'casts', 'file']

        ##order by field
        ordering = ['ranking']


class MovieDetailSerializer(serializers.ModelSerializer):
    """a class for serializing a specific movie data in order to show that
    single movie full information"""

    ##cast's id that has played in movie
    casts = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        """class Meta"""

        ##model
        model = Movie

        ##fields
        fields = ['id', 'movie_name', 'ranking', 'rating', 'price', 'last_modification', 'jdatetime',
                  'casts', 'file', 'tooman']

        ##ordering by field
        ordering = ['ranking']


class MovieDownloadlinkSerializer(serializers.ModelSerializer):
    """serilizer class for creating the movie downloadlink"""

    ##creating download link with serializer methodfield
    downloadlink_with_methodfield = serializers.SerializerMethodField()

    ##creating download link with serializer model method
    downloadlink_with_modelmethod = serializers.CharField(source='get_download_link')

    @extend_schema_field(OpenApiTypes.ANY)
    def get_downloadlink_with_methodfield(self, obj):
        """create download link dynamically with serializer method field

        @param self :)
        @param obj movie instance that we want to create a download link for that

        @return download link (String)
        """
        return 'http://127.0.0.1:8000/' + obj.file

    class Meta:
        """class meta"""

        ##model
        model = Movie

        ##fields
        fields = ['download_link', 'downloadlink_with_methodfield', 'downloadlink_with_modelmethod']


class MovieUpdateSerializer(serializers.ModelSerializer):
    """a serializer class for updating an existing movie information"""

    ##datetime field with persian jalali calendar
    jdatetime = JDateTimeField()

    class Meta:
        """class meta"""

        ##model
        model = Movie

        ##fields
        fields = ['movie_name', 'ranking', 'rating', 'price', 'file', 'jdatetime', 'last_modification']
