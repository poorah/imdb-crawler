##
# @file movie/signals.py
#
# @brief movie signals
#
# @section description_movie Description
# movie app post save signal for sending email to users after creating or modifying a movie
#
##
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import timezone
from datetime import timedelta
from .models import Movie


@receiver(post_save, sender=Movie)
def send_notification_email(sender, instance, **kwargs):
    """
    a signal function that sends notification email after creating or modifying a movie

    @param sender model class
    @param instance object that has been saved right now

    @return void it will send email using django core mail
    """
    subject = 'movies updated!!'
    context = {'movie_name': instance.movie_name}
    message = render_to_string('movie/mail.html', context)
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ['pouyarahmaneian@gmail.com', ]
    send_mail(subject, message, email_from, recipient_list, fail_silently=False)
    print('email sent!')

    ##comparing jalali date format and Gregorian format
    import pytz
    tz = pytz.timezone('Asia/Tehran')

    print(f"difference of '{timezone.now()}' and ", instance.jdatetime, ':',
          timezone.now() - instance.jdatetime)

    print(f"difference of '{timezone.now()}' - ", instance.last_modification,
          timezone.now() - instance.last_modification)

    print(f"difference of ", instance.jdatetime, " - ", instance.last_modification,
          instance.jdatetime - instance.last_modification)

    print("times orm:",
          Movie.objects.filter(jdatetime__gt=timezone.now() - timedelta(minutes=2)))
