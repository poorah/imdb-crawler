##
# @file movie/tests.py
#
# @brief movie tests
#
# @section description_movie Description
# movie app unit test cases
#
# @section libraries_movie Libraries/Modules
#  - Django rest_framework.
##
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from .models import Movie

class MovieTest(APITestCase):
    """a class for movie app unit tests"""
    def setUp(self):
        """initializing functions that create a new movie for test

        @param self :)
        @return void
        """
        self.movie = Movie(
            movie_name='test',
            ranking=1,
            rating=2.5,
            price=10000,
            download_link="http://test.test"
        )
        self.movie.save()

    def test_show_all_movies(self):
        """
        show all movie unit test

        @param self
        @return void
        """
        url = reverse('movie_view')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_show_movie_detail(self):
        """
        show a movie detail unit test

        @param self
        @return void
        """
        url = reverse('movie_detail', args=(self.movie.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_movie(self):
        """
        delete a movie unit test

        @param self
        @return void
        """
        url = reverse('movie_detail', args=(self.movie.id,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)