##
# @file movie/urls.py
#
# @brief movie urls
#
# @section description_movie Description
# movie app urls
#
##
from django.urls import path
from .views import MovieView, MovieDetail, MovieDownloadLink

urlpatterns = [
    ##showing all the movie with brief data as a list
    path('showall/', MovieView.as_view(), name='movie_view'),

    ##showing a movie with complete information
    path('detail/<int:pk>', MovieDetail.as_view(), name='movie_detail'),

    ##movies download link
    path('downloadlink/<int:pk>', MovieDownloadLink.as_view(), name='movie-downloadlink')
]
