##
# @file movie/views.py
#
# @brief movie views
#
# @section description_movie Description
# movie app views
#
# @section libraries_movie Libraries/Modules
#  - Django rest_framework.
#  - oauth2_provider
##
from rest_framework import generics
from .models import Movie
from .serializer import MovieSerializer, MovieDetailSerializer, MovieDownloadlinkSerializer
from .pernissions import HasUserBoughtVideo, JustDestroyPermission
from rest_framework.response import Response
from rest_framework import status
from .serializer import MovieUpdateSerializer
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope
from django.utils import timezone


class MovieView(generics.ListAPIView):
    """a view class using Generic ListAPIView for showing movies list """

    queryset = Movie.objects.all()
    serializer_class = MovieSerializer

    ##custom permission class
    #permission_classes = [TokenHasReadWriteScope]


class MovieDetail(generics.RetrieveUpdateDestroyAPIView):
    """a view class using Generic "RetrieveUpdateDestroyAPIView" for showing a specific
      movie with detail"""

    queryset = Movie.objects.all()
    serializer_class = MovieDetailSerializer
    permission_classes = [JustDestroyPermission]

    def update(self, request, pk, *args, **kwargs):
        """
        overriding update method in order to use another serializer

        @param self :)
        @param request incoming request
        @param pk movie's id
        @return updated datas or serializer errors
        """
        try:
            movie = Movie.objects.get(id=pk)
            request.data['last_modification'] = timezone.now()
            request.data['jdatetime'] = timezone.now()
            serializer = MovieUpdateSerializer(movie, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response("movie not found!", status=status.HTTP_404_NOT_FOUND)


class MovieDownloadLink(generics.RetrieveAPIView):
    """a class for showing user download link"""

    queryset = Movie.objects.all()
    serializer_class = MovieDownloadlinkSerializer

    ##custom permission that checks if user has bought the video
    permission_classes = [HasUserBoughtVideo]
