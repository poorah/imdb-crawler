from django.urls import path
from .views import TestApiAuth

urlpatterns = [
    # /o/ for testing oauth2
    path('', TestApiAuth.as_view(), name='testAuth')
]