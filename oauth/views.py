##
# @file oauth/views.py
#
# @brief oauth views
#
# @section description_oauth Description
# a view for oauth app that is for test whether oauth is working or not
#
##
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response


class TestApiAuth(APIView):
    """a class for testing if authentication is working correctly or not"""

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        """
         a function for GET request method
         @param self :)
         @param request incoming request

         @return a Response
         """
        return Response("you've passed the authentication :-D")
