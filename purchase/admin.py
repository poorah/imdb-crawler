##
# @file purchase/admin.py
#
# @brief purchase admin configs
#
# @section description_purchase Description
# purchase model admin panel configs
#
# @section libraries_purchase Libraries/Modules
#  - Django.contrib.admin
##

from django.contrib import admin
from .models import Purchase


class UserAdminPermission(admin.ModelAdmin):
    """a custom permission class"""
    def has_add_permission(self, request, obj=None):
        """a function that checks if the user has can add permission or not

        @param self
        @param request incoming request
        @param obj current object
        @return True/False
        """
        return request.user.has_perm('purchase.add_purchase')

    def has_change_permission(self, request, obj=None):
        """a function that checks if the user has 'purchase.add_purchase' permission or not

        @param self
        @param request incoming request
        @param obj current object
        @return True/False
        """

        if not request.user.has_perm('purchase.change_price'):
            self.readonly_fields = ('price',)
        return True


admin.site.register(Purchase, UserAdminPermission)
