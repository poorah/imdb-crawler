##
# @file purchase/models.py
#
# @brief purchase models
#
# @section description_purchase Description
# purchase model definition
#
##
from django.db import models
from movie.models import Movie
from django.core.validators import MinValueValidator
from django.contrib.auth import get_user_model

User = get_user_model()


class Purchase(models.Model):
    """Purchase model definition"""

    user = models.ForeignKey(User, related_name='purchase', on_delete=models.PROTECT)
    movie = models.ManyToManyField(Movie, related_name='purchase')
    price = models.IntegerField(validators=[MinValueValidator(0)])
    ##creation date
    date = models.DateTimeField()

    # todo: add permission and use user.has_perm()
    class Meta:
        """class meta"""

        ##custom permissions
        permissions = [
            ('can_add_purchase', 'can create a new purchase'),
            ('change_price', 'can change price')
        ]

    def __str__(self):
        """
        __str__ function
        @param self

        @return user's id who has done the purchase plus price of the purchase
        """
        return str(self.id) + ' -> price: ' + str(self.price)
