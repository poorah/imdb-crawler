##
# @file purchase/permissions.py
#
# @brief purchase permissions
#
# @section description_purchase Description
# purchase app custom permissions
#
##
from rest_framework import permissions


class CanAddPurchase(permissions.BasePermission):
    """a class for checking can_add_purchase permission"""
    def has_permission(self, request, view):
        """
        a function that checks whether user has permission to create a new purchase or not

        @param self
        @param request incoming request
        @param view

        @return True/False
        """

        if request.method == 'POST':
            user = request.user
            if user.has_perm('purchase.can_add_purchase'):
                return True
            return False
        return True


class HasWriteScope(permissions.BasePermission):
    """a class that checks the incoming token scope """
    def has_permission(self, request, view):
        """
        a function that checks whether user has permission to perform Write methods(POST, PUT) or not.

        @param self
        @param request incoming request
        @param view

        @return True/False
        """
        write_methods = ['PUT', 'POST']
        if request.method in write_methods:
            token = request.auth
            if not token or not hasattr(token, 'scope'):
                return False
            return 'write' in getattr(token, 'scope')
        return True
