##
# @file purchase/serializer.py
#
# @brief purchase serializer
#
# @section description_purchase Description
# purchase app serializers
#
##
from rest_framework import serializers
from .models import Purchase
from user.serializer import UserSerializer
from movie.models import Movie


class PurchaseSerializer(serializers.ModelSerializer):
    """
    a serializer class for showing all purchases with brief information
    """
    class Meta:
        """class meta"""

        ##model
        model = Purchase

        ##fields
        fields = ['id', 'user', 'movie', 'price', 'date']


class PurchaseDetailView(serializers.ModelSerializer):
    """a serializer class for showing a specific purchase with complete information"""

    ##user's data that has done this purchase
    user = UserSerializer()

    ##movie's data that has been bought
    movie = serializers.PrimaryKeyRelatedField(many=True, queryset=Movie.objects.all())

    class Meta:
        """class meta"""

        ##model
        model = Purchase

        ##fields
        fields = ['id', 'user', 'movie', 'price', 'date']


class PurchaseUpdateSerializer(serializers.ModelSerializer):
    """a serializer for updating an existing purchase data"""
    class Meta:
        """class Meta"""

        model = Purchase
        fields = ['user', 'movie', 'price', 'date']


class PurchaseCreateSerializer(serializers.ModelSerializer):
    """a serializer to create a new purchase(buy some movies)"""
    class Meta:
        """class Meta"""

        model = Purchase
        fields = ['user', 'movie', 'price', 'date']
