from django.urls import path
from .views import PurchaseView, PurchaseDetailView


urlpatterns = [
    path('', PurchaseView.as_view(), name='PurchaseView'),
    path('detail/<int:pk>', PurchaseDetailView.as_view(), name='PurchaseView')
]