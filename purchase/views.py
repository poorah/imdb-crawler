##
# @file purchase/views.py
#
# @brief purchase views
#
# @section description_purchase Description
# purchase app views
#
##
from rest_framework import generics
from .models import Purchase
from .serializer import PurchaseSerializer, PurchaseDetailView, PurchaseUpdateSerializer, \
    PurchaseCreateSerializer
from rest_framework import permissions, status
from rest_framework.response import Response
from movie.models import Movie
from django.db.models import Sum
from .permissions import CanAddPurchase, HasWriteScope
from django.contrib.auth.hashers import make_password


class PurchaseView(generics.ListCreateAPIView):
    """a class for viewing the list of purchases and creating a new one using ListCreateAPIView"""

    ##queryset
    queryset = Purchase.objects.all()

    ##serializer class
    serializer_class = PurchaseSerializer

    ##permission classes
    permission_classes = [permissions.IsAuthenticated,
                          CanAddPurchase]

    def create(self, request, *args, **kwargs):
        """
        overriding create function

        @param self :)
        @param request incoming request

        @return new purchase data or serializer errors if datas were not valid
        """

        price = Movie.objects.filter(id__in=request.data['movie']).aggregate(price=Sum('price'))
        request.data['price'] = price['price']
        serializer = PurchaseCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PurchaseDetailView(generics.RetrieveUpdateDestroyAPIView):
    """a class for viewing the list of purchases and creating a new one using ListCreateAPIView"""

    ##queryset
    queryset = Purchase.objects.all()

    ##serializer class
    serializer_class = PurchaseDetailView

    ##permission classes
    permission_classes = [permissions.IsAuthenticated
                          # HasWriteScope
                          ]

    def update(self, request, pk, *args, **kwargs):
        """
        overriding update method in order to use PurchaseUpdateSerializer

        @param self :)
        @param request incoming request
        @param pk movie's id
        @return updated datas or serializer errors
        """
        try:
            purchase = Purchase.objects.get(pk=pk)
            serializer = PurchaseUpdateSerializer(purchase, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response("purchase not found!", status=status.HTTP_404_NOT_FOUND)

# todo :- add presave => ok
# todo :- change db to postgres => ok
# todo :- jwt auth => ok
# ------------------------
# todo: filefield and pathfilefield => ok
# todo :-  method field to make url => ok
# todo: add permission to delete without overridng =>ok
# todo: custom validator => ok
# todo: add to function to model => ok
# -------------------------
# todo: all auth (oauth (oidc ldap) token session jwt => ok
# todo: bind serializer with a method in model serializer method fields are just for validation,.. none related works to model=> ok
# todo: read existing permissions => ok
# todo: model User groups and custom permissions => try user groups => ok
# --------------------------
# todo: method field to make url => ok
# todo: add permission to delete without overridng =>ok?? talk
# todo: custom validator => ok
# todo: add to function to model => ok where we can use that??(admin,..)
# todo: id field in updates deleted => ok
# -------------------------
# todo: all auth (oaut,ok) (oidc,ok,) (ldap,ok,) (token,ok) (session,ok,) (jwt,ok)
# todo: implement (ldap=>linux) session (token=ok) (oauth=ok)
# todo: implement two more oauth => ok
# todo: more oauth in depth (grant types=>ok) (scope=>ok in purchase permission)
# todo: read more about refresh token => ok
# todo: read about headers => ok
# todo: add test api for authorization in oauth => ok
# todo: read oauth in postman => ok
# todo: ldap mocking server => needs linux => comment implementation => ok
# todo: config simplejwt => done
# todo: read custom authentication,manager,abstract user : email and mobilephone as username
# todo: implement session/cookie => ok
# todo: extending user => ok
# todo: read how to run an oidc with react id token,access token, => read library how they communicate with oidc provider => ok
# -------------------------------
# todo: read ou dc in ldap => ok
# todo: how to document python => Doxygen , sphinx, api documenting => ok
# todo: add description to postman => ok do more
# todo: add multiple settings.py for production using environment variable => ok
# todo: add onenote to gitlab => ok
# todo: push codes into gitlab => ok
# todo: add custom exceptions for django rest complete => ok
# todo: read swagger docs => ok
# -------------------------------------------
# todo: read django translation => ok
# todo: update gitlab readme => ~~
# todo: send email => ok
# --------------------------------------------
# todo: jalali date in views and admin => bug in jalalifield
# todo: timezone in database utc => ok check movies/signals
# todo: compare multiple timezones => ok
# todo: manage.py setting environment => ok
# todo: update documentation => ok
# ---------------------------------------------
# todo: add throttle => ok
# todo: change last modification into jalali
# todo: why django orm gte is correct but comparing not
# ----------------------------------------------------------
# todo: custom model field => ok
# todo: in git: create a change source code => ok
