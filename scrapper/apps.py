##
# @file scrapper/configs.py
#
# @brief scrapper configs
#
# @section description_scrapper Description
# scrapper app configs
#
##
from django.apps import AppConfig


class ScrapperConfig(AppConfig):
    """
    Scrapper configs
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'scrapper'

    def ready(self):
        """
        ready function to run signals when occurred and run apschedular

        @param self :)

        @return void
        """
        from .scheduler import scheduel
        scheduel()

        import scrapper.signals