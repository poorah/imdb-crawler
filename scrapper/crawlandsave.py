##
# @file scrapper/crawlandsave.py
#
# @brief scrapper crawler function
#
# @section description_scrapper Description
# the function that crawl the IMDB Top chart, download the trailer from YouTube and save datas in database
#
# @section libraries_scrapper Libraries/Modules
#  - youtubedownloader
##
from .scrapper import crawl
from movie.models import Movie
from cast.models import Cast
from .youtubedownloader import download_trailer
from django.utils import timezone
from scrapper.cutomexception import LowResoultionException
import os
from django.conf import settings


def crawlandsave():
    """
    the function that crawl the IMDB Top chart, download the trailer from YouTube and save datas in database

    @return void
    """

    ##calling the crawl() function to crawl the IMDB Top Chart and return data in movies list
    movies = crawl()
    i = 1

    ##iterate over movies list items and save data in database
    for movie in movies:
        ##check if the movie already exist in database or not
        ##if it was in databse update it
        if Movie.objects.filter(movie_name=movie['movie_title']).exists():
            print(f"{movie['movie_title']} found!")
            m = Movie.objects.get(movie_name=movie['movie_title'])
            m.ranking = movie['ranking']
            m.rating = round(float(movie['rating']), 2)
            m.last_modification = timezone.now()
            m.save()
            i += 1
        ##if movie was not in database create a new one
        else:
            print(f"{movie['movie_title']} not found!")
            try:
                print(f'downloading {i}/{len(movies)}...')
                resoulotion = download_trailer(movie['movie_title'])

                newMovie = Movie(
                    ranking=int(movie['ranking']),
                    movie_name=movie['movie_title'],
                    rating=round(float(movie['rating']), 2),
                    price=10000,
                    resouloution=resoulotion,
                    download_link="http://127.0.0.1:8000/static/{filename} Video.mp4".format(
                        filename=movie['movie_title']),
                    file=os.path.join(settings.MOVIE_PATH, "{filename} Video.mp4".format(filename=movie['movie_title']))
                )
                try:
                    newMovie.save()

                    casts = movie['star_cast'].split(', ')
                    for cast in casts:
                        cast_name = " ".join(cast.split(" ")[:2])
                        if not Cast.objects.filter(cast_name=cast_name).exists():
                            newcast = Cast(
                                cast_name=cast_name
                            )
                            newcast.save()
                            newMovie.casts.add(newcast)
                        else:
                            newMovie.casts.add(Cast.objects.get(cast_name=cast_name))
                except LowResoultionException:
                    ##pre save signal will check the movie resolution if resolution is not 720p
                    ##it will prevent to save in database
                    print('saving failed becuase of low resoultion')
            except:
                print('downloading {movie} failed'.format(movie=movie['movie_title']))
            i += 1
    print("done")
