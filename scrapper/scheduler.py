##
# @file scrapper/scheduler.py
#
# @brief scrapper scheduel function
#
# @section description_scrapper Description
# scheduling crawladnsave function to be performed every day at 8:00
#
# @section libraries_scrapper Libraries/Modules
#  - apscheduler
##
from apscheduler.schedulers.background import BackgroundScheduler
from .crawlandsave import crawlandsave


def scheduel():
    """using background scheduling to run on a separate thread"""
    sched = BackgroundScheduler()
    # sched.add_job(crawlandsave, 'cron', day_of_week='sat', hour=00, minute=00)
    sched.add_job(crawlandsave, 'cron', hour=8, minute=00)
    sched.start()
