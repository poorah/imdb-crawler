##
# @file scrapper/scrapper.py
#
# @brief crawl function
#
# @section description_scrapper Description
# the function that crawls IMDB Top chart and save it into database
#
# @section libraries_scrapper Libraries/Modules
#  - BeautifulSoup
#  - requests
##
from bs4 import BeautifulSoup
import requests
import re


def crawl():
    """
    crawl http://www.imdb.com/chart/top, parse data as a dictionary for each movie
    and put all movie's information in list. return that list

    @return movie's data list
    """

    url = 'http://www.imdb.com/chart/top'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    movies = soup.select('td.titleColumn')
    cast = [a.attrs.get('title') for a in soup.select('td.titleColumn a')]
    ratings = [b.attrs.get('data-value')
               for b in soup.select('td.posterColumn span[name=ir]')]

    list = []

    for index in range(0, len(movies)):
        movie_string = movies[index].get_text()
        movie = (' '.join(movie_string.split()).replace('.', ''))
        movie_title = movie[len(str(index)) + 1:-7]
        year = re.search('\((.*?)\)', movie_string).group(1)
        ranking = movie[:len(str(index)) - (len(movie))]
        data = {"ranking": ranking,
                "movie_title": movie_title,
                "rating": ratings[index],
                "year": year,
                "star_cast": cast[index],
                }
        list.append(data)
    return list
