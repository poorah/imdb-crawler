##
# @file scrapper/signals.py
#
# @brief scrapper signals
#
# @section description_scrapper Description
# a presave signal that checks video resolution
#
#
##

from django.db.models.signals import pre_save
from movie.models import Movie
from django.dispatch import receiver
from .cutomexception import LowResoultionException


@receiver(pre_save, sender=Movie)
def check_resoulution(sender, instance, **kwargs):
    """
    a function that checks the video resolution if resolution is not 720p
    it will prevent to save it

    @param sender
    @param instance
    @return void
    """

    print('in signal')
    if instance.resouloution != "720p":
        print(f'{instance.resouloution} is lower than 720p')
        raise LowResoultionException
    else:
        print(f'resolution is {instance.resouloution} => ok')
# todo: use that set instance hint and customize exception => ok
