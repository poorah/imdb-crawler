##
# @file scrapper/youtubedownloader.py
#
# @brief YouTube trailer download function
#
# @section description_scrapper Description
# the function that downloads the movie trailer
#
# @section libraries_scrapper Libraries/Modules
#  - youtubesearchpython
#  - pytube
##
from youtubesearchpython import VideosSearch
from pytube import YouTube


def download_trailer(movie_name):
    """
    trailer downloader from YouTube function

    @param movie_name name of the movie

    @return resolution [String]
    """
    videosSearch = VideosSearch(f'{movie_name} trailer', limit=1)

    result = videosSearch.result()
    link = result['result'][0]['link']
    print('{link} found'.format(link=link))

    print('wait...')
    yt = YouTube(link)
    file = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()
    resolution = file.resolution
    file.download(r"C:\Users\HP\Desktop\IMDBCrawler\movie\static", filename=f'{movie_name} Video.mp4')
    print('done')
    return resolution
