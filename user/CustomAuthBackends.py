##
# @file user/CustomAuthBackends.py
#
# @brief Custom Authentication class
#
# @section description_user Description
# a Custom authentication class that users can log in with their email or phone number
#
##
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import BaseBackend
from django.db.models import Q

User = get_user_model()


class EmailOrPhoneAuthBackend(BaseBackend):
    """a custom authentication class that provides a way for users to log in
    with their email or phone number"""

    def authenticate(self, request, **kwargs):
        """
        custom authentication function
        @param self
        @param request

        @return user -> if authentication was successful, None -> if not
        """
        try:
            ##when user is logging in via an api
            username = request.data['username']
            password = request.data['password']
        except:
            ##when user is logging in via template(e.g. admin panel,...)
            username = request.POST['username']
            password = request.POST['password']
        if not username or not password:
            return None

        try:
            user = User.objects.get(Q(email=username) | Q(phone_number=username))
            if user.check_password(password):
                return user
            return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        """
        get user function

        @param self
        @param user_id id of the user

        @return user -> if authentication was successful, None -> if not
        """
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
