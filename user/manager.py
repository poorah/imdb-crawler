##
# @file user/CustomAuthBackends.py
#
# @brief Custom manager class for user model
#
# @section description_user Description
# a Custom manager class for user model that overrides create_user and create_superuser functions
#
##
from django.contrib.auth.models import UserManager


class CustomManager(UserManager):
    """
    a Custom manager class for user model
    that overrides create_user and create_superuser functions
    """
    def create_user(self, email, password, phone_number):
        """
        overriding the create_user function in order to create a new user with email and phone number

        @param self
        @param email user email
        @param password user password
        @param phone_number user phone number

        @return user instance
        """
        if not email:
            raise ValueError('email is required')
        user = self.model(
            email=email,
            phone_number=phone_number
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, phone_number):
        """
        overriding the create_superuser function inorder to create a superuser with email and phone number

        @param self
        @param email user email
        @param password user password
        @param phone_number user phone number

        @return user instance
        """
        user = self.create_user(email, password, phone_number)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user
