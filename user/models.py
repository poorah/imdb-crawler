##
# @file user/models.py
#
# @brief Custom user model class
#
# @section description_user Description
# a Custom user model class that extends the django's default user model
#
##
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinLengthValidator
from .manager import CustomManager


class CustomUser(AbstractUser):
    """custom user model that extends django default model"""
    email = models.EmailField(unique=True, max_length=256)
    phone_number = models.CharField(max_length=11, validators=[MinLengthValidator(11)],
                                    unique=True)
    username = models.CharField(unique=False, max_length=255)
    objects = CustomManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']
