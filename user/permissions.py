##
# @file user/permissions.py
#
# @brief Custom user permissions
#
# @section description_user Description
#     a custom permission class that checks whether user has permission
#     to edit or delete an object or not
#
##
from rest_framework import permissions


class IsOwnerOrForbidden(permissions.BasePermission):
    """
    a custom permission class that checks whether user has permission
    to edit or delete an object or not
    """

    def has_object_permission(self, request, view, obj):
        """
        this function prevents users to access other users information
        @param self
        @param request
        @param view
        @param obj

        @return True/False
        """
        return request.user.id == obj.id
