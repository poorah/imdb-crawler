##
# @file user/serializer.py
#
# @brief Custom user serializers
#
# @section description_user Description
# custom user serializers
#
##
from rest_framework import serializers
from purchase.models import Purchase
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """
    a serializer for showing all the users with complete information as a list
    """
    class Meta:
        """class meta"""
        model = User
        fields = '__all__'


class UserDetailSerializer(serializers.ModelSerializer):
    """a serializer class for showing a specific user complete information"""

    ##show purchases id that user has done
    purchase = serializers.PrimaryKeyRelatedField(many=True, queryset=Purchase.objects.all())

    class Meta:
        """class meta"""

        ##model
        model = User

        ##fields
        fields = ['id', 'username', 'first_name', 'last_name', 'email', 'password', 'is_staff',
                  'is_active', 'is_superuser', 'last_login', 'date_joined', 'purchase']


class CreateUserSerializer(serializers.ModelSerializer):

    class Meta:

        model = User

        fields = ['email', 'phone_number', 'password']
