from django.urls import path
from .views import UserListCreate, UserDetailView, Login


urlpatterns = [
    path('', UserListCreate.as_view(), name='UserListCreate'),
    path('detail/<int:pk>', UserDetailView.as_view(), name='UserDetail'),
    path('login/', Login.as_view(), name='CustomLogin')
]