##
# @file user/views.py
#
# @brief Custom user views
#
# @section description_user Description
# custom user views
#
##
from rest_framework import generics
# from django.contrib.auth.models import User
from .serializer import UserSerializer, UserDetailSerializer
from rest_framework import permissions
from .permissions import IsOwnerOrForbidden
from django.contrib.auth import get_user_model
from rest_framework.views import APIView
from django.contrib.auth import login, authenticate
from rest_framework.response import Response
from rest_framework import status
from drf_spectacular.utils import extend_schema
from .serializer import CreateUserSerializer
from django.contrib.auth.hashers import make_password

User = get_user_model()


class UserListCreate(generics.ListCreateAPIView):
    """a class for showing users as a list and create new user using ListCreateAPIView"""

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated,
                          permissions.IsAdminUser]

    def create(self, request, *args, **kwargs):
        """
        overridng create method

        @param self
        @param request

        @return Response
        """

        request.data['password'] = make_password(request.data['password'])
        serializer = CreateUserSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    """a class for showing a specific user, update or delete
    it using RetrieveUpdateDestroyAPIView"""

    queryset = User.objects.all()
    serializer_class = UserDetailSerializer
    permission_classes = [permissions.IsAuthenticated,
                          IsOwnerOrForbidden]


class Login(APIView):
    """a login class-based view implemented session/cookie authentication"""
    def post(self, request):
        """
        authenticating(session/cookie method) user function

        @param self
        @param request

        @return Response
        """

        user = authenticate(request)
        if user is not None:
            login(request, user)
            return Response({"msg": "welcome"})
        return Response({"error": "login failed!"}, status=status.HTTP_400_BAD_REQUEST)

